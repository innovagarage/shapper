package com.interitumsoftware.mertovski.wirelessnotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertemir on 03/12/15.
 */
public class MyService extends Service {
    ListView lv;
    WifiManager wifi;
    String wifis[];
    ArrayList<String> list = new ArrayList<String>();
    // WifiScanReceiver wifiReciever;
    Button notif;
    TextView timer;
    int a = 20, b = 20, z = 20, y = 20;
    static Boolean durdur;
    int t = 1, r = 1, e = 1, w = 1;
    private String[] wantedSSIDS = new String[5];

    String hotspot = "Connectify-mcdonalds";
    String hotspot1 = "Batuhan Iphone";
    String subway = "Sagca";
    String bibercafe = "biberburger";
    String pericafe = "pericafe";
    String akademikuafor = "Akademi Kuafor";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {

        MainActivity m = new MainActivity();

        Log.d("DEMO LOG", "onClick: starting srvice");

        wantedSSIDS[0] = "Subway";
        wantedSSIDS[1] = "biberburger";
        wantedSSIDS[2] = "Akademi Kuafor";
        wantedSSIDS[3] = "pericafe";
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);


        //wifiReciever = new WifiScanReceiver();

        //lv=(ListView)findViewById(R.id.listView);

        // lv.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, wifis));

        scanWireless();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void Goster()
    {
        wifi.startScan();

        List<ScanResult> wifiScanList = wifi.getScanResults();
        wifis = new String[wifiScanList.size()];
        for(int i = 0; i < wifiScanList.size(); i++){

            wifis[i] = ((wifiScanList.get(i).SSID).toString());
           // Toast.makeText(this, wifis[i] , Toast.LENGTH_LONG).show();

            if(wifiScanList.get(i).SSID.toString().contains(hotspot1))
                {
                    wifiScanList.clear();
                    if(a==20){
                        wifiScanList.clear();
                        zamanlayıcıSubway();
                        wifiScanList.clear();
                        a = 5;
                    }
                }

                else if(wifis[i].contains(bibercafe))
                {
                    if(b==20){
                        zamanlayıcıBiber();
                        b = 5;
                    }
                }

                else if(wifis[i].contains(pericafe))
                {
                    if(z==20){
                        zamanlayıcıPeri();
                        z = 5;
                    }
                }

                else if(wifis[i].contains(akademikuafor))
                {
                    if(y==20){
                        zamanlayıcıKuafor();
                        y = 5;
                    }
                }
        }
    }

    Handler h = new Handler();

    private void scanWireless() {
        h.postDelayed(new Runnable() {
            public void run() {
                //wifi.startScan();
                Goster();
                Log.d("scan başladı", "onClick: starting srvice");
                //Toast.makeText(getApplicationContext(),"New Scan",Toast.LENGTH_SHORT).show();
                if(durdur)
                scanWireless();
            }
        }, 5 * 1000);
    }

//    public class WifiScanReceiver extends BroadcastReceiver{
//
//        public void onReceive(Context c, Intent intent) {
//
//            List<ScanResult> wifiScanList = wifi.getScanResults();
//            wifis = new String[wifiScanList.size()];
//
//            for(int i = 0; i < wifiScanList.size(); i++){
//
//                wifis[i] = ((wifiScanList.get(i)).toString());
//
////                for(int x =0; x < wantedSSIDS.length; x++ )
////                {
////                    if(wifis[i].contains(wantedSSIDS[x]))
////                    {
////
////                    }
////
////                }
//
//                if(wifis[i].contains(subway))
//                {
//                    if(a==20){
//                        zamanlayıcıSubway();
//                        a = 5;
//                    }
//                }
//
//                else if(wifis[i].contains(bibercafe))
//                {
//                    if(b==20){
//                        zamanlayıcıBiber();
//                        b = 5;
//                    }
//                }
//
//                else if(wifis[i].contains(pericafe))
//                {
//                    if(z==20){
//                        zamanlayıcıPeri();
//                        z = 5;
//                    }
//                }
//
//                else if(wifis[i].contains(akademikuafor))
//                {
//                    if(y==20){
//                        zamanlayıcıKuafor();
//                        y = 5;
//                    }
//                }
//
//                else if(wifis[i].contains(hotspot))
//                {
//                    // Build notification
//                    // Actions are just fake
//                    Notification noti = new Notification.Builder(getApplicationContext())
//                            .setContentTitle("You have entered the wifi")
//                            .setContentText(hotspot).setSmallIcon(R.mipmap.ic_launcher).build();
//                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                    // hide the notification after its selected
//                    noti.flags |= Notification.FLAG_AUTO_CANCEL;
//
//                    notificationManager.notify(0, noti);
//
//                }
//            }
//
//            lv.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, wifis));
//        }
//    }

    private void zamanlayıcıSubway() {

        final NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle("McDonalds'da fırsat!");

       // final Bitmap aBigBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mcd_banner);

        final RemoteViews expandedView = new RemoteViews(this.getPackageName(),
                R.layout.notif_layout);

       // notiStyle.bigPicture(aBigBitmap);

        if(t == 1)
        {
            // Build notification
            // Actions are just fake
                Notification noti = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle("McDonalds'da fırsat!")
                        .setSmallIcon(R.mipmap.mcdonalds)
                   //     .setStyle(notiStyle)
                        .build();

            noti.contentView = expandedView;
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(0, noti);
                t = 2;
        }

        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                // Build notification
                // Actions are just fake
                Notification noti = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle("McDonalds'da fırsat!")
                        .setSmallIcon(R.mipmap.mcdonalds)
                      //  .setStyle(notiStyle)
                        .build();

                noti.contentView = expandedView;
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    // hide the notification after its selected
                    noti.flags |= Notification.FLAG_AUTO_CANCEL;

                    notificationManager.notify(0, noti);
                    a = 20;
            }

        }.start();
    }

    private void zamanlayıcıKuafor() {

        if(r == 1)
        {
            // Build notification
            // Actions are just fake
            Notification noti = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Kuaför")
                    .setContentText("Kuaför").setSmallIcon(R.mipmap.ic_launcher).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(0, noti);
            r = 2;
        }

        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                // Build notification
                // Actions are just fake
                Notification noti = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Kuaför")
                        .setContentText("Kuaför").setSmallIcon(R.mipmap.ic_launcher).build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(0, noti);
                y=20;
            }

        }.start();
    }

    private void zamanlayıcıPeri() {

        if(e == 1)
        {
            // Build notification
            // Actions are just fake
            Notification noti = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Peri Cafe")
                    .setContentText("Peri Cafe").setSmallIcon(R.mipmap.ic_launcher).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(0, noti);
            e = 2;
        }

        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                // Build notification
                // Actions are just fake
                Notification noti = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Peri Cafe")
                        .setContentText("Peri Cafe").setSmallIcon(R.mipmap.ic_launcher).build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(0, noti);
                b=20;
            }

        }.start();
    }

    private void zamanlayıcıBiber() {

        if(w == 1)
        {
            // Build notification
            // Actions are just fake
            Notification noti = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Biber Burger")
                    .setContentText("Biber Burger").setSmallIcon(R.mipmap.ic_launcher).build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(0, noti);
            w = 2;
        }

        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                // Build notification
                // Actions are just fake
                Notification noti = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Biber Burger")
                        .setContentText("Biber Burger").setSmallIcon(R.mipmap.ic_launcher).build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(0, noti);
                y=20;
            }

        }.start();
    }



}
